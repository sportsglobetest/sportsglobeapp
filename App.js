import 'react-native-gesture-handler';
import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {Tabs} from './src/Tabs';

const App = () => {
  return (
    <NavigationContainer>
      <StatusBar backgroundColor={'#D5202B'} />
      <Tabs />
    </NavigationContainer>
  );
};

export default App;
