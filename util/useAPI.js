import {useEffect, useReducer} from 'react';
import axios from 'axios';

/**
 * This hook is responsible for making api calls.
 * @param {string} url holds the url to be called.
 * @returns A state object with a status, data and error (null of no error).
 */
export const useAPI = url => {
  const baseUrl =
    'https://digital-staging.sportingglobe.com.au/wp-json/members/';

  const initialState = {
    status: 'idle',
    error: null,
    data: [],
  };

  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'FETCHING':
        return {...initialState, status: 'fetching'};
      case 'FETCHED':
        return {...initialState, status: 'fetched', data: action.payload};
      case 'FETCH_ERROR':
        return {...initialState, status: 'error', error: action.payload};
      default:
        return state;
    }
  }, initialState);

  useEffect(() => {
    const fetchData = async () => {
      dispatch({type: 'FETCHING'});
      try {
        const data = await axios.get(baseUrl + url);
        dispatch({type: 'FETCHED', payload: data.data});
      } catch (error) {
        dispatch({type: 'FETCH_ERROR', payload: error.message});
      }
    };
    fetchData();
  }, [url]);
  return state;
};
