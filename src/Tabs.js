import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Empty} from './Empty';
import {OffersNav} from './OffersNav';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export const Tabs = () => {
  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator
      tabBarOptions={{
        style: {
          backgroundColor: '#D5202B',
          height: 100,
          borderTopColor: '#D5202B',
          borderTopWidth: 0,
        },
        activeTintColor: '#FFF',
        labelStyle: {marginBottom: 30, fontSize: 15, fontWeight: 'bold'},
        inactiveTintColor: '#c5b2b3',
      }}>
      <Tab.Screen
        name="Home"
        component={Empty}
        options={{
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="shield-outline"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Offers"
        options={{
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="offer" color={color} size={size} />
          ),
        }}
        component={OffersNav}
      />
      <Tab.Screen
        name="Tipping"
        options={{
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="checkbox-marked-outline"
              color={color}
              size={size}
            />
          ),
        }}
        component={Empty}
      />
      <Tab.Screen
        name="Book"
        options={{
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="silverware-fork-knife"
              color={color}
              size={size}
            />
          ),
        }}
        component={Empty}
      />
      <Tab.Screen
        name="More"
        options={{
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="menu" color={color} size={size} />
          ),
        }}
        component={Empty}
      />
    </Tab.Navigator>
  );
};
