import React from 'react';
import {Offer} from './Offer';
import {OfferList} from './OfferList';
import {createStackNavigator} from '@react-navigation/stack';

export const OffersNav = () => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator initialRouteName="Offers">
      <Stack.Screen
        name="Offers"
        component={OfferList}
        options={{
          title: 'My Offers',
          headerStyle: {
            backgroundColor: '#D5202B',
          },
          headerTintColor: '#FFFFFF',
          headerTitleStyle: {alignSelf: 'center'},
        }}
      />
      <Stack.Screen
        name="Details"
        component={Offer}
        options={({route}) => ({
          title: route.params.name,
          headerStyle: {
            backgroundColor: '#D5202B',
          },
          headerTintColor: '#FFFFFF',
        })}
      />
    </Stack.Navigator>
  );
};
