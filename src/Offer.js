import React from 'react';
import {StyleSheet, ScrollView, Text, View, Image} from 'react-native';
export const Offer = ({route}) => {
  const {offer} = route.params;
  return (
    <ScrollView>
      <View style={styles.offer}>
        <Image
          style={styles.offerImage}
          source={{
            uri: `${offer.image}`,
          }}
        />
        <View style={styles.offerContent}>
          <Text style={styles.header}>{offer.title}</Text>
          <Text style={styles.subHeader}>Offer Sub Heading</Text>
          <Text style={styles.normalText}>
            Offer description, Offer description, Offer description, Offer
            description, Offer description, Offer description
          </Text>
          <Text style={styles.subHeader}>Offer Sub Heading</Text>
          <Text style={styles.normalText}>
            Offer description, Offer description, Offer description, Offer
            description, Offer description, Offer description
          </Text>
          {offer.limited === true && (
            <Text style={styles.subHeader}>LIMITED TIME ONLY</Text>
          )}
          <Text style={styles.subHeader}>VALID AT</Text>
          <Text style={styles.normalText}>{offer.locations}</Text>
          <Text style={styles.subHeader}>Scan the barcode to redeem</Text>
          <Image style={styles.barcode} source={require('./img/barcode.png')} />
        </View>
      </View>
    </ScrollView>
  );
};
const width_proportion = '100%';
const styles = StyleSheet.create({
  header: {
    color: '#ffffff',
    fontSize: 23,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  subHeader: {
    color: '#ffffff',
    fontSize: 23,
    fontWeight: 'bold',
  },
  normalText: {
    color: '#ffffff',
    fontSize: 20,
    marginBottom: 20,
  },
  offer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#292929',
  },
  offerImage: {
    marginBottom: 12,
    width: width_proportion,
    height: 127,
    resizeMode: 'cover',
  },
  offerContent: {
    alignItems: 'center',
    marginRight: 60,
    marginLeft: 60,
  },
  barcode: {
    backgroundColor: 'white',
    alignSelf: 'stretch',
    width: width_proportion,
    height: 200,
    flex: 1,
  },
});
