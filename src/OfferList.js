import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  ActivityIndicator,
  View,
  Image,
  TouchableHighlight,
} from 'react-native';
import {useAPI} from '../util/useAPI';

export const OfferList = ({navigation}) => {
  const {status, data, error} = useAPI(
    'get/offers?user_id=25879&location=Geelong',
  );
  const [offers, setOffers] = useState([]);
  useEffect(() => {
    if (status === 'fetched') {
      setOffers(data);
    }
  }, [status]);
  const offerMenu = () => {
    return (
      <>
        {status === 'fetching' && (
          <View style={[styles.container, styles.horizontal]}>
            <ActivityIndicator size="large" color="white" />
          </View>
        )}
        {status === 'error' && (
          <Text style={{fontSize: 20, color: 'white'}}>{error}</Text>
        )}
        {status === 'fetched' && (
          <ScrollView style={styles.scrollView}>
            {offers.length !== 0 &&
              offers.map((offer, index) => (
                <View key={index} style={styles.container}>
                  <TouchableHighlight
                    onPress={() => {
                      navigation.navigate('Details', {
                        offer: offer,
                        name: offer.title,
                      });
                    }}>
                    <Image
                      key={index}
                      style={styles.stretch}
                      source={{
                        uri: `${offer.image}`,
                      }}
                      resizeMode="stretch"
                    />
                  </TouchableHighlight>
                </View>
              ))}
          </ScrollView>
        )}
      </>
    );
  };

  return <View style={styles.app}>{offerMenu()}</View>;
};

const styles = StyleSheet.create({
  scrollView: {
    width: '95%',
  },
  stretch: {
    width: '100%',
    height: 127,
    marginTop: 12,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  app: {
    backgroundColor: '#292929',
    flex: 1,
    alignItems: 'center',
  },
});
